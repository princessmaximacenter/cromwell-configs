Cromwell Configurations Files
=============================

This repository contains the Cromwell configurations that are used by the HPC Cromwell-as-a-Service.

## ONE version: ONE config
There is a single configuration per cromwell version. Each user of that cromwell version gets THAT configuration. For references the default configuration is added here to have an overview of the defaults.

## How it is used on the Cromwell-as-a-Service
The HPC admin team creates a template out of these configurations. Based on this single template multiple configuration files are; one for each user. This enables secure setting of database credentials, cromwell-execution directories, et cetera.

The Cromwell-as-a-service will use the configuration to start cromwell in a systemctl unit as follows:
```
/usr/bin/java -Dconfig.file=cvanrun-cromwell-38.conf
	-Dwebservice.port=10003 \
	-Ddatabase.profile="slick.jdbc.MySQLProfile$" \
	-Ddatabase.db.driver="com.mysql.cj.jdbc.Driver" \
	-Ddatabase.db.url="jdbc:mysql://localhost:3306/cvanrun?rewriteBatchedStatements=true" \
	-jar cromwell-35.jar server'
```

## Creating a new configuration
The following should be taken into consideration when creating a new configuration:

* Add the default reference configuration. For v38 this can be found here: https://raw.githubusercontent.com/broadinstitute/cromwell/38/core/src/main/resources/reference.conf
* Prefix `# HPC note: the following should be overwritten on the command-line call to start the cromwell instance` to any variable you expect the HPC admins to overwrite.
* Disable the Local backend by adding `Local.config.root: /dev/null/THE_LOCAL_BACKEND_IS_DISABLED_YOU_NAUGHTY_HUMAN` to `backend.providers`
