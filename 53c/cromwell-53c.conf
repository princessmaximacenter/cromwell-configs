# This line is required. It pulls in default overrides from the embedded cromwell `application.conf` needed for proper
# performance of cromwell.
include required(classpath("application"))

# Cromwell HTTP server settings
webservice {
   # HPC note: the following should be overwritten on the command-line call to start the cromwell instance
  #port = 8098
  interface = 127.0.0.1
  binding-timeout = 5s
   # HPC note: the following should be overwritten on the command-line call to start the cromwell instance
  instance.name = "cromwell-v53-hpc-DEFAULT"
}

# Cromwell "system" settings
system {
  # Cromwell will cap the number of running workflows at N
  max-concurrent-workflows = 200

  # Cromwell will launch up to N submitted workflows at a time, regardless of how many open workflow slots exist
  max-workflow-launch-count = 20

  # Number of seconds between workflow launches
  new-workflow-poll-rate = 10

  # Since the WorkflowLogCopyRouter is initialized in code, this is the number of workers
  number-of-workflow-log-copy-workers = 10

  # Default number of cache read workers
  number-of-cache-read-workers = 20

  io {
    # Global Throttling - This is mostly useful for GCS and can be adjusted to match
    # the quota availble on the GCS API
    number-of-requests = 100000
    per = 100 seconds

    # Number of times an I/O operation should be attempted before giving up and failing it.
    number-of-attempts = 5
  }

  input-read-limits {
    lines = 64000000
    bool = 14
    int = 38
    float = 100
    string = 64000000
    json = 64000000
    tsv = 64000000
    map = 64000000
    object = 64000000
  }

  job-rate-control {
    jobs = 5
    per = 1 second
  }
}

# Optional call-caching configuration.
call-caching {
  # Allows re-use of existing results for jobs you've already run
  # (default: false)
  enabled = true

  # Whether to invalidate a cache result forever if we cannot reuse them. Disable this if you expect some cache copies
  # to fail for external reasons which should not invalidate the cache (e.g. auth differences between users):
  # (default: true)
  invalidate-bad-cache-results = true
}

workflow-options {
  # Directory where to write per workflow logs
   # HPC note: the following should be overwritten on the command-line call to start the cromwell instance
  #workflow-log-dir: "workflow-logs"
  workflow-failure-mode: "ContinueWhilePossible"
  workflow-log-temporary: false
}

backend {
  # Override the default backend.
  default = "SLURM"


  # The list of providers.
  providers {

    # Disables the Local backend, unfortunately this will still present it as 'supported' when the API call is made for supported backends
    Local.config.root: /dev/null/THE_LOCAL_BACKEND_IS_DISABLED_YOU_NAUGHTY_HUMAN

    SGE {
      actor-factory = "cromwell.backend.impl.sfs.config.ConfigBackendLifecycleActorFactory"
      config {
        # Root directory where Cromwell writes job results.  This directory must be
        # visible and writeable by the Cromwell process as well as the jobs that Cromwell
        # launches.
        # HPC note: the following should be overwritten on the command-line call to start the cromwell instance
        # root = "cromwell-executions"

        # By default the ConfigBackendLifecycleActorFactory puts sync at the end of
        # every job, this slightly over the top and might be overloading I/O to
        # the Isilon storage on the HPC.
        script-epilogue=""

        exit-code-timeout-seconds = 1800
        # File system configuration.
        filesystems {

          # For SFS backends, the "local" configuration specifies how files are handled.
          local {

            # Try soft-link (ln -s), if fail, copy the files.
            localization: [
              "soft-link", "copy"
            ]

            # Call caching strategies
            caching {
              # When copying a cached result, what type of file duplication should occur. Attempted in the order listed below:
              duplication-strategy: [
                "soft-link", "copy"
              ]

              # Possible values: file, path
              # "file" will compute an md5 hash of the file content.
              # "path" will compute an md5 hash of the file path. This strategy will only be effective if the duplication-strategy (above) is set to "soft-link",
              # in order to allow for the original file path to be hashed.
              hashing-strategy: "path+modtime"

              # When true, will check if a sibling file with the same name and the .md5 extension exists, and if it does, use the content of this file as a hash.
              # If false or the md5 does not exist, will proceed with the above-defined hashing strategy.
              check-sibling-md5: true
            }
          }
	      }

	      # The defaults for runtime attributes if not provided.
        default-runtime-attributes {
          failOnStderr: false
          continueOnReturnCode: 0
        }

        # Limits the number of concurrent jobs
        concurrent-job-limit = 400

        temporary-directory = "$(echo $TMPDIR)"

        runtime-attributes = """
	      String sge_pe = "threaded"
        Int? cpu = 1
        Float? memory_gb = 12
	      Float? tmpspace_gb = 4
        String? wallclock
        String? sge_project
        String? sge_queue
        String? node_exclude
	      String? docker
	      String? instance_name
        """

        submit = """
          qsub \
          -terse \
          -notify \
          -b n \
          -N ${job_name} \
          -wd ${cwd} \
          -o ${out} \
          -e ${err} \
          ${if cpu>1 then "-pe " + sge_pe + " " + cpu else " "} \
          ${"-l h_vmem=" + memory_gb + "G"} \
          ${"-l tmpspace=" + tmpspace_gb + "G"} \
          ${"-l h_rt=" + wallclock } \
          ${"-l h=" + '"!(' + node_exclude + ')"'} \
          ${"-q " + sge_queue} \
          ${"-P " + sge_project} \
          ${script}
        """

        kill = "qdel ${job_id}"

        check-alive = """
          stderr=$(qstat -j "${job_id}" 3>&2 2>&1 1>&3) # Flipping around stdout <-> stderr fd's
          original_exit_code="$?"
          echo "$stderr"
          if [[ "$stderr" == *"timeout error"* ]]; then
            exit 0;  # This is Fine.
          else
            exit "$original_exit_code";
          fi
        """

        job-id-regex = "(\\d+)"
      }
    },

    SLURM {
      actor-factory = "cromwell.backend.impl.sfs.config.ConfigBackendLifecycleActorFactory"
      config {
        # Root directory where Cromwell writes job results.  This directory must be
        # visible and writeable by the Cromwell process as well as the jobs that Cromwell
        # launches.
        # HPC note: the following should be overwritten on the command-line call to start the cromwell instance
        # root = "cromwell-executions"

        # By default the ConfigBackendLifecycleActorFactory puts sync at the end of
        # every job, this slightly over the top and might be overloading I/O to
        # the Isilon storage on the HPC.
        # In addition, a known but is that slurmstepd CAN output things to stderr but this is
        # only a benign error. It does interact with the runtime' failOnStderr
        script-epilogue="sed -i '/^slurmstepd.*is_a_lwp.*proc.*failed.*No such.*$/d' stderr"

        exit-code-timeout-seconds = 600
        # File system configuration.
        filesystems {

          # For SFS backends, the "local" configuration specifies how files are handled.
          local {

            # Try cached-copy, which copies the target once and will use hard links
            # Try soft-link (ln -s), if fail, copy the files.
            localization: [
              "soft-link", "copy"
            ]

            # Call caching strategies
            caching {
              # When copying a cached result, what type of file duplication should occur. Attempted in the order listed below:
              duplication-strategy: [
                "soft-link", "copy"
              ]

              # Possible values: file, path
              # "file" will compute an md5 hash of the file content.
              # "path" will compute an md5 hash of the file path. This strategy will only be effective if the duplication-strategy (above) is set to "soft-link",
              # in order to allow for the original file path to be hashed.
              hashing-strategy: "path+modtime"

              # When true, will check if a sibling file with the same name and the .md5 extension exists, and if it does, use the content of this file as a hash.
              # If false or the md5 does not exist, will proceed with the above-defined hashing strategy.
              check-sibling-md5: true
            }
          }
        }

        # The defaults for runtime attributes if not provided.
        default-runtime-attributes {
          failOnStderr: false
          continueOnReturnCode: 0
        }

        # Limits the number of concurrent jobs
        concurrent-job-limit = 400

        temporary-directory = "$(echo $TMPDIR)"

        runtime-attributes = """
          Int? cpu = 1
          Float? memory_gb = 12
          Float? tmpspace_gb = 4
          String? wallclock = "10:00"
          String? node_exclude
          String? slurm_account
          String? slurm_partition
          # If partition is not explicitly specified the task_type will guide
          # which partition will be used

          String? task_type = "compute"

          # Some large runtime knobs to turn if a sample gets stuck
          Int? cpu_multiplier = 1
          Float? memory_multiplier = 1.0
          Float? wallclock_multiplier = 1.0
          Float? tmpspace_multiplier = 1.0

          # Container attributes
          String? docker
          String? bind
          String? container_shell
        """

        # With they way that SGE is setup, we need to do cpu*2 to get similar
        # number of available threads
        submit = """
          task_type=${task_type}
          case $task_type in
            compute)
              spartition="cpu"
              ;;
            gpu_accelerated)
              spartition="gpu"
              ;;
            transfer)
              spartition="xfr"
              ;;
          esac

          if [ -z "${slurm_partition}" ];
          then
            slurm_partition="$spartition"
          else
            slurm_partition="${slurm_partition}"
          fi
          # enable the use of the wallclock modifier by getting requested seconds
          # do so by reverse traveling over the wallclock items and summate them
          runtime_minutes=$(python3 <<CODE
import re
wallclock=re.split(':|-', "${wallclock}")
wallclock.reverse()
multipliers = [1, 60, 3600, 86400]
total = 0
for t,m in zip(wallclock, multipliers):
  total = total + int(t)*m
print(int(total * ${wallclock_multiplier} / 60))
CODE
)

          sbatch \
            --job-name=${job_name} \
            --partition=$slurm_partition \
            --chdir=${cwd} \
            --output=${out} \
            --error=${err} \
            --cpus-per-task=${cpu * 2 * cpu_multiplier} \
            --time=$runtime_minutes \
            --mem=${ ceil(memory_gb * memory_multiplier) }G \
            --gres=tmpspace:${ ceil(tmpspace_gb * tmpspace_multiplier)}G \
            ${"--exclude=" + node_exclude} \
            ${"--account=" + slurm_account} \
            --get-user-env=L \
            --wrap "/usr/bin/bash --login ${script}"
        """

	submit-docker = """
	    source $HOME/.bashrc

			task_type=${task_type}
          case $task_type in
            compute)
              spartition="cpu"
              ;;
            gpu_accelerated)
              spartition="gpu"
              ;;
            transfer)
              spartition="xfr"
              ;;
          esac

          if [ -z "${slurm_partition}" ];
          then
            slurm_partition="$spartition"
          else
            slurm_partition="${slurm_partition}"
          fi
          # enable the use of the wallclock modifier by getting requested seconds
          # do so by reverse traveling over the wallclock items and summate them
          runtime_minutes=$(python3 <<CODE
import re
wallclock=re.split(':|-', "${wallclock}")
wallclock.reverse()
multipliers = [1, 60, 3600, 86400]
total = 0
for t,m in zip(wallclock, multipliers):
  total = total + int(t)*m
print(int(total * ${wallclock_multiplier} / 60))
CODE
)

      # Make sure the APPTAINER_(LOCAL)CACHEDIR variable is set. If not use a default
      # based on the users home.
      if [ -z "$APPTAINER_CACHEDIR" ]; then
        if [ ! -z "$SINGULARITY_CACHEDIR" ]; then
          export APPTAINER_CACHEDIR="$SINGULARITY_CACHEDIR";
        else
          export APPTAINER_CACHEDIR="$HOME";
        fi;
      fi;

      echo "---------------------------------------"
      echo "I will use $APPTAINER_CACHEDIR as Singularity/Apptainer Cache directory."
      echo "To change this behaviour please add the following line to your ~/.bashrc file:"
      echo "export APPTAINER_CACHEDIR=MY_CACHE_DIR"
      echo "and replace MY_CACHE_DIR with the full path you wish to use as cache dir."
      echo "---------------------------------------"
      echo

      # choose job shell
      if [ -z "${container_shell}" ]; then
        echo "I found no container_shell argument. I will use default ${job_shell} to start the script in the container."
        echo
        export JOBSHELL="${job_shell}";
      else
        echo "Default container_shell (${job_shell}) is overwritten with ${container_shell}."
        echo
        export JOBSHELL="${container_shell}";
      fi;


      export APPTAINER_CACHEDIR="$APPTAINER_CACHEDIR"
      export APPTAINER_LOCALCACHEDIR="$APPTAINER_CACHEDIR"

      export BINDCOMMAND=""

      # Collect all locations that needs to be attached to the container
      if [ -n "${bind}" ]; then
        # temporary change the separator to , instead if spaces to handle spaces in paths
        OIFS="$IFS"
        IFS=","

        # Create the arg call to use later in the singularity call
        for local_path in "${bind}"; do
          if [ ! -f "$local_path" ]; then
            echo "ERROR: File $local_path not found!"
            echo 1 > ${cwd}/execution/rc
            exit 1;
          fi;
          BINDCOMMAND="--bind $local_path:$local_path $BINDCOMMAND";
        done;

        # set back the IFS and export the bind command
        export IFS="$OIFS"
        export BINDCOMMAND
      else
        export BINDCOMMAND=""
      fi

		  sbatch \
            --job-name=${job_name} \
            --partition=$slurm_partition \
            --chdir=${cwd} \
            --output=${out} \
            --error=${err} \
            --cpus-per-task=${cpu * 2 * cpu_multiplier} \
            --time=$runtime_minutes \
            --mem=${ ceil(memory_gb * memory_multiplier) }G \
            --gres=tmpspace:${ ceil(tmpspace_gb * tmpspace_multiplier)}G \
            ${"--exclude=" + node_exclude} \
            ${"--account=" + slurm_account} \
            --get-user-env=L \
			      --wrap 'apptainer exec --writable-tmpfs --containall --env TMPDIR=/tmp --bind $TMPDIR:/tmp $BINDCOMMAND \
			      --bind ${cwd}:${docker_cwd} docker://${docker} $JOBSHELL ${docker_script} || echo $? > ${cwd}/execution/rc'
      """

        kill = "scancel ${job_id}"

        check-alive = """
          stderr=$(squeue -j "${job_id}" 3>&2 2>&1 1>&3) # Flipping around stdout <-> stderr fd's
          original_exit_code="$?"
          echo "$stderr"
          if [[ "$stderr" == *"timed out"* ]]; then
            exit 0;  # This is Fine.
          else
            exit "$original_exit_code";
          fi
        """

        job-id-regex = "Submitted batch job (\\d+).*"
      }
    }
  }
}

database {
  db {
    # HPC note: the following should be overwritten on the command-line call to start the cromwell instance
    password="{{ item.pass }}"
    connectionTimeout = 10000
  }
}
